import os
import sys
import numpy as np
import tensorflow as tf
from tensorflow.keras import models, layers
from tensorflow.keras.datasets import mnist
from skimage import io


sample_side_len = 28
sample_channels = 1
amt_classes = 10
# amount of times repeated trainings using the complete dataset
epochs = 50
neurons_hidden_layer_1 = 16
neurons_hidden_layer_2 = 32
neurons_hidden_layer_3 = 64
neurons_hidden_layer_4 = 128
neurons_hidden_layer_5 = 128
filter_size = (5, 5)
dropout_rate_conv_layer = 0.15
dropout_rate_dense = 0.20
dropout_seed = 69
pooling_window = (2, 2)
activation = 'relu'
checkpoint_path = "model/trained.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)


def load_data():
    (training_samples, training_labels), (test_samples,
                                          test_labels) = mnist.load_data()

    training_labels = training_labels.astype(int)
    test_labels = test_labels.astype(int)
    training_samples = training_samples.astype(np.float32)
    test_samples = test_samples.astype(np.float32)

    # explicitly reshape to have 4th dimension mentioned
    training_samples = training_samples.reshape(
        [-1, sample_side_len, sample_side_len, sample_channels])
    test_samples = test_samples.reshape(
        [-1, sample_side_len, sample_side_len, sample_channels])

    # Normalize pixel intensity of input samples/images from [0-255] to [0-1].
    training_samples = training_samples / 255.0
    test_samples = test_samples / 255.0

    return (training_samples, training_labels, test_samples, test_labels)


def load_image():
    while True:
        try:
            file_path = input("Path to image: ")
            image = io.imread(file_path, as_gray=True)
            image = np.array(
                [image.reshape((sample_side_len, sample_side_len, 1))])
            image = 1.0 - image.astype(np.float32)
            return image
        except ValueError as err:
            print("Image cannot be resized to 28x28, try another.")
        except FileNotFoundError as err:
            print("File not found.")


def main():
    training_samples, training_labels, test_samples, test_labels = load_data()

    model = models.Sequential()
    model.add(layers.Conv2D(neurons_hidden_layer_1, filter_size, padding="same", activation=activation,
                            input_shape=(sample_side_len, sample_side_len, sample_channels)))
    model.add(layers.MaxPooling2D(pooling_window))
    model.add(layers.Dropout(dropout_rate_conv_layer, seed=dropout_seed))
    model.add(layers.Conv2D(neurons_hidden_layer_2, filter_size, padding="same"))
    model.add(layers.MaxPooling2D(pooling_window))
    model.add(layers.Dropout(dropout_rate_conv_layer, seed=dropout_seed))
    model.add(layers.Conv2D(neurons_hidden_layer_3, filter_size, padding="same"))
    model.add(layers.Conv2D(neurons_hidden_layer_4, filter_size, padding="same"))
    model.add(layers.Conv2D(neurons_hidden_layer_5, filter_size, padding="same"))
    model.add(layers.Flatten())
    model.add(layers.Dense(neurons_hidden_layer_5, activation=activation))
    model.add(layers.Dropout(dropout_rate_dense, seed=dropout_seed))
    model.add(layers.Dense(amt_classes, activation='softmax'))
    model.compile(optimizer='sgd',
                  loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    # train
    if (len(sys.argv) == 2 and sys.argv[1] == 'train'):
        checkpoint = tf.keras.callbacks.ModelCheckpoint(
            filepath=checkpoint_path, save_weights_only=True)
        history = model.fit(training_samples, training_labels,
                            epochs=epochs, validation_data=(test_samples, test_labels), callbacks=[checkpoint])
    # or load
    else:
        model.load_weights(checkpoint_path)

    # eval
    # loss, accuracy = model.evaluate(test_samples, test_labels, verbose=2)
    # print("accuracy: " + str(accuracy))

    # predict
    while True:
        image = load_image()
        predictions = model.predict(image)
        print("This is: " +
              str(np.unravel_index(predictions.argmax(), predictions.shape)[1]))


main()
